/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "cocostudio\CocoStudio.h"

using namespace cocostudio::timeline;

std::string fishAnimName[3] = {
	"ikan_biru.csb", "ikan_kuning.csb", "ikan_orange.csb"
};

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    visibleSize = Director::getInstance()->getVisibleSize();
    designResolutionSize = AppDelegate::designResolutionSize;
    addX = (visibleSize.width - designResolutionSize.width) / 2;
    addY = (visibleSize.height - designResolutionSize.height) / 2;

    auto background = Sprite::create("Asset/bg.png");
    background->setPosition(visibleSize / 2);
    this->addChild(background);

	int fishName = random(0, 2);

    auto terumbuKarang = Sprite::create("Asset/terumbu.png");
    terumbuKarang->setPosition(visibleSize.width / 2, 223 + addY);
    this->addChild(terumbuKarang, 1);

	auto fishAnim = CSLoader::createNode(fishAnimName[fishName]);
	ActionTimeline* action = CSLoader::createTimeline(fishAnimName[fishName]);
	action->gotoFrameAndPlay(0, true);
	fishAnim->runAction(action);
	fishAnim->setPosition(200, 200);
	this->addChild(fishAnim, 1);

    title = Sprite::create("Asset/judul.png");
    title->setPosition(visibleSize.width / 2, 525 + addY);
    this->addChild(title, 1);

    btnPlay = Button::create("Asset/play.png", "Asset/play.png", "Asset/play.png");
    btnPlay->setPosition(Vec2(visibleSize.width / 2, 178 + addY));
    btnPlay->setScale(0.8);
    btnPlay->runAction(RepeatForever::create(Sequence::create(DelayTime::create(5), EaseSineInOut::create(SkewTo::create(0.5, 5, 5)), EaseSineInOut::create(SkewTo::create(0.5, -5, -5)), EaseElasticOut::create(SkewTo::create(1, 0, 0)), NULL)));
    this->addChild(btnPlay, 1);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

    this->schedule(schedule_selector(HelloWorld::bubble), 3);

    return true;
}

void HelloWorld::bubble(float dt) {
    for (int i = 0; i < 4; i++) {
        int posX = random(0, (int)visibleSize.width);

        for (int j = 0; j < i; j++) {
            auto gelembung = Sprite::create("Asset/bubble.png");
            gelembung->setPosition(posX + random(-30, 30) + addX, 30 + addY);
            gelembung->setScale(random(0.3f, 0.4f));
            gelembung->runAction(Sequence::create(DelayTime::create(0.2f * j), MoveTo::create(5, Vec2(gelembung->getPosition().x, visibleSize.height + 100)), NULL));
            //gelembung->runAction(RepeatForever::create(Sequence::create(EaseSineInOut::create(MoveBy::create(1, Vec2(25, 0))), EaseSineInOut::create(MoveBy::create(1, Vec2(-25, 0))), NULL))->reverse());
            this->addChild(gelembung);

			dataBubble.pushBack(gelembung);
        }
    }
}

bool HelloWorld::onTouchBegan(Touch* touch, Event* event) {
	for (int i = 0; i < dataBubble.size(); i++) {
		if (dataBubble.at(i)->getBoundingBox().containsPoint(touch->getLocation())) {
			auto bubbleParticle = ParticleSystemQuad::create("bubble_particle.plist");
			bubbleParticle->setPosition(dataBubble.at(i)->getPosition());
			bubbleParticle->setAutoRemoveOnFinish(true);
			this->addChild(bubbleParticle);

			dataBubble.at(i)->removeFromParent();
			dataBubble.erase(dataBubble.begin() + i);
		}
	}

	return true;
}